import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import QuestsScreen from './QuestsScreen';
import AdminScreen from './AdminScreen';

class Login extends Component {
  constructor(props){
    super(props);
    this.state={
      username:'',
      password:'',
      isAdmin: false
    }
  }
  render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
            <AppBar
              title="Login"
            />
            <TextField
              hintText="Enter your Email"
              floatingLabelText="Email"
              onChange = {(event,newValue) => this.setState({username:newValue})}
              />
            <br/>
            <TextField
              type="password"
              hintText="Enter your Password"
              floatingLabelText="Password"
              onChange = {(event,newValue) => this.setState({password:newValue})}
            />
            <br/>
            <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }

  handleClick(event){
    var apiBaseUrl = "http://localhost:4000/users/";
    var self = this;
    var payload={
      "email":this.state.username,
      "password":this.state.password
    }
    axios.post(apiBaseUrl+'login', payload)
    .then(function (response) {
      //console.log(response);
      if(response.status === 200){
        localStorage.setItem('token', response.data.token);
        var isAdmin=response.data.isAdmin;
        if(isAdmin){
          var adminScreen=[];
          adminScreen.push(<AdminScreen appContext={self.props.appContext}/>);
          self.props.appContext.setState({loginPage:[],questsScreen:[],adminScreen:adminScreen});
        }
        else{     
          var questsScreen=[];
          questsScreen.push(<QuestsScreen appContext={self.props.appContext}/>);
          self.props.appContext.setState({loginPage:[],questsScreen:questsScreen,adminScreen:[]});
        }
      }
      else if(response.status === 204){
        console.log("Email and password do not match");
        alert("Email and password do not match")
      }
      else{
        console.log("Email does not exist");
        alert("Email does not exist");
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}

const style = {
  margin: 15,
};

export default Login;