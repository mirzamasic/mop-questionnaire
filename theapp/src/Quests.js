import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import Logout from './Logout';

class Quests extends Component {
	constructor(props) {
		super(props);

		this.state = {
			questionnaires: [],
			singleQuestShow: false,
			singleQuestId: '',
			singleQuest: [],
			singleQuestQuestions: [],
			questions: [],
			questMessage: '',
			questMessageColor: ''
		};

		this.takeQuestionnaire = this.takeQuestionnaire.bind(this);
	};

	componentDidMount() {
		var apiBaseUrl = "http://localhost:4000/users/";
		
		axios.post(apiBaseUrl+'getQuests', null, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then(results => {
			//console.log(results);
			return results;
		})
		.then(data => {
			let questionnaires = data.data.data.map((quest) => {
				//console.log(JSON.stringify(quest));
				return(
					<div key={quest.id} id={'quest-'+quest.id} className="quest-single">
						<div>
							<h4><b>Naslov: {quest.title}</b></h4>
							<p className="text-muted">{quest.description}</p>
						</div>
						<Button variant="contained" color="primary" onClick={(e) => this.takeQuestionnaire(quest.id, e)}>
					    	Take it
					    </Button>
					</div>
				)
			})
			this.setState({questionnaires: questionnaires});
			//console.log("state", this.state.questionnaires);
		})
		.catch(function (error) {
	      	console.log(error);
	    });
	}

	takeQuestionnaire(id, e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      "questId":id,
	    }

	    axios.post(apiBaseUrl+'getSingleQuest', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
    	.then((response) => {
    		if(response.status === 200){
	    		this.setState({
	    			singleQuestShow: true,
	    			singleQuestId: id,
	    			singleQuest: response.data.questionnaire,
	    			singleQuestQuestions: response.data.questions
	    		});
    		}
    		else if(response.status === 204){
    			this.setState({
    				singleQuestShow: false,
    				questMessage: 'You have already completed that questionnaire.',
    				questMessageColor: 'red'
    			})
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});
	}

	goBack(e) {
		this.setState({
			singleQuestShow: false,
			singleQuest: [],
			singleQuestQuestions: [],
			questMessage: ''
		})
	}

	singleOnChange(e, id) {
		let newQuestions = this.state.questions;
		newQuestions['question-'+id]=e.target.value;
		this.setState({ 
			questions: newQuestions
		});

		console.table(this.state);
	}

	textOnChange(e, id) {
		let newQuestions = this.state.questions;
		newQuestions['question-'+id]=e.target.value;
		this.setState({ 
			questions: newQuestions
		});

		console.table(this.state);
	}

	submitAnswers() {
		var apiBaseUrl = "http://localhost:4000/users/";

		var oldAnswers = this.state.questions;

		var answers = Object.keys(oldAnswers).map(function(key) {
			return [key, oldAnswers[key]];
		});
		
		var payload = {
			questId: this.state.singleQuestId,
			answers: answers
		}

		axios.post(apiBaseUrl+'saveAnswers', payload, {
			headers: { 
				'token': localStorage.getItem('token'),
			}
		})
		.then((response) => {
			if(response.status === 200){
	    		this.setState({
	    			singleQuestShow: false,
	    			singleQuestId: '',
	    			singleQuest: [],
	    			singleQuestQuestions: [],
	    			questMessage: 'Your answers have been successfully saved to database. Thank you.',
	    			questMessageColor: 'green'
	    		});
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});
	}

	render() {
		if(this.state.singleQuestShow) {
			return (
				<MuiThemeProvider>
					<AppBar
						position="static"
			        	title={this.state.singleQuest.title}
			        >
			        	<Toolbar>
			        		<Button 
			        			variant="contained" 
			        			color="primary"
			        			onClick={(e) => this.goBack(e)}
			        			style={{marginRight: '10px'}}
			        		>
			        			Back
			        		</Button>
		        			<Logout/>
			        	</Toolbar>
			        </AppBar>
			        <div className="container" style={{marginTop: '20px'}}>
			        	<h4><span className="text-muted"><i>Description: </i></span>{this.state.singleQuest.description}</h4>
			        	<hr/>
			        	{this.state.singleQuestQuestions.map((a) => {
			        		this.state.questions.concat({['question-'+a.id]: ''});
			        		if(a.type==="single"){
								return (
									<div>
										<FormControl component="fieldset" required style={{marginTop: '20px'}}>
											<FormLabel component="legend">{a.question}</FormLabel>
											<RadioGroup
												aria-label={a.question}
												name={'question-'+a.id}
												onChange={(e) => this.singleOnChange(e,a.id)}
												value={this.state.questions['question-'+a.id]}
											>
												{a.answers.map((e) => {
						        					return (
														<FormControlLabel value={String(e.id)} control={<Radio />} label={e.answer} />
													);
												})}
											</RadioGroup>
										</FormControl>
			        				</div>
								);
							}
							else if(a.type==="text"){
								return (
									<div>
										<FormControl component="fieldset" required style={{marginTop: '20px'}}>
											<FormLabel component="legend">{a.question}</FormLabel>
											<TextField
									          	id="name"
									         	label="Enter an answer"
									          	value={this.state.questions['question-'+a.id]}
									          	onChange={(e) => this.textOnChange(e,a.id)}
									          	margin="normal"
									        />
										</FormControl>
									</div>
								);
							}
							else if(a.type==="yes-no"){
								return (
									<div>
										<FormControl component="fieldset" required style={{marginTop: '20px'}}>
											<FormLabel component="legend">{a.question}</FormLabel>
											<RadioGroup
												aria-label={a.question}
												name={'question-'+a.id}
												onChange={(e) => this.singleOnChange(e,a.id)}
												value={this.state.questions['question-'+a.id]}
											>
												<FormControlLabel value='yes' control={<Radio />} label='Yes' />
												<FormControlLabel value='no' control={<Radio />} label='No' />
											</RadioGroup>
										</FormControl>
									</div>
								);
							}
			        	})}
			        	<hr />
			        	<Button 
        					variant="contained" 
        					color="primary"
        					onClick={(e) => this.submitAnswers(e)}
        				>
        					Submit
        				</Button>
			        </div> 
			    </MuiThemeProvider>
		    )
		}
		else {
			return (
				<MuiThemeProvider>
					<AppBar
			        	title="Questionnaires"
			        >
			        <Toolbar>
		        		<Logout/>
		        	</Toolbar>
			        </AppBar>
					<div className="quest-container container">
						<div>
							<p style={{'color': this.state.questMessageColor, marginTop: '20px'}}>{this.state.questMessage}</p>
						</div>
						{this.state.questionnaires}
					</div>
				</MuiThemeProvider>
			)
		}
	}
}

export default Quests;