import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Quests from './Quests';

class QuestsScreen extends Component {
	constructor(props) {
		super(props);

		this.state={
			questsscreen:[]
		}
	}

	componentWillMount(){
		var questsscreen = [];
		questsscreen.push(<Quests parentContext={this} appContext={this.props.parentContext}/>);
		this.setState({
			questsscreen: questsscreen
		})
	}

	render() {
		return (
			<div className="questsscreen">
				{this.state.questsscreen}
			</div>
		)
	}
}

export default QuestsScreen;