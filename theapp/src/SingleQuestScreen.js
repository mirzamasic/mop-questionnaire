import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Quests from './Quests';

class SingleQuestScreen extends component {
	constructor(props) {
		super(props);

		this.state={
			singlequestscreen:[]
		}
	}

	componentWillMount(){
		var singlequestscreen = [];
		singlequestscreen.push(<SingleQuest parentContext={this} appContext={this.props.parentContext}/>);
		this.setState({
			singlequestscreen: singlequestscreen
		})
	}

	render() {
		return (
			<div className="singlequestscreen">
				{this.state.singlequestscreen}
			</div>
		)
	}
}

export default SingleQuestsScreen;