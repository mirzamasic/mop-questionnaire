import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Loginscreen from './Loginscreen';
import injectTapEventPlugin from 'react-tap-event-plugin';
import axios from 'axios';
import QuestsScreen from './QuestsScreen';
import AdminScreen from './AdminScreen';

injectTapEventPlugin();

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      loginPage:[],
      questsScreen:[],
      adminScreen:[]
    }
  }
  componentWillMount(){
    let token = localStorage.getItem('token');
    if(typeof token !== 'undefined' && token!== null){
      var self = this;
      var apiBaseUrl = "http://localhost:4000/users/";
      var payload = {
        token: token
      }
      axios.post(apiBaseUrl+'checkSession', payload)
      .then(function (response) {
        if(response.status === 200){
          var isAdmin=response.data.isAdmin;
          if(isAdmin){
            var adminScreen=[];
            adminScreen.push(<AdminScreen appContext={self.props.appContext}/>);
            self.setState({loginPage:[],questsScreen:[],adminScreen:adminScreen});
          }
          else{     
            var questsScreen=[];
            questsScreen.push(<QuestsScreen appContext={self.props.appContext}/>);
            self.setState({loginPage:[],questsScreen:questsScreen,adminScreen:[]});
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    }
    else {
      var loginPage =[];
      loginPage.push(<Loginscreen parentContext={this}/>);
      this.setState({
        loginPage:loginPage,
        questsScreen:[],
        adminScreen:[]
      })
    }
  }
  render() {
    return (
      <div className="App">
        {this.state.loginPage}
        {this.state.questsScreen}
        {this.state.adminScreen}
      </div>
    );
  }
}

const style = {
  margin: 15,
};

export default App;
