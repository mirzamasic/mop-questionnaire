import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Button from '@material-ui/core/Button';
import Logout from './Logout';
import Toolbar from '@material-ui/core/Toolbar';
import axios from 'axios';
import TextField from 'material-ui/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class AdminScreen extends Component {
	constructor(props) {
		super(props);

		this.state={
			adminHome: true,
			addQ: false,
			deleteQ: false,
			questionnaires: [],
			questMessage: '',
			questMessageColor: '',
			showQuest: true,
			showQuestion: false,
			showAnswer: false,
			questId: '',
			questionId: '',
			questTitle: '',
			questDescription: '',
			question: '',
			typeOfQuestion: '',
			answerSingle: '',
			answers: [],
			answersFormated: [],
			addAnswers: false,
			newQuestionnaireMessage: ''
		}
	}

	addQuestionnaire(e) {
		this.setState({adminHome: false,addQ:true,deleteQ:false});
	}

	deleteQuestionnaire(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
		this.setState({questionnaires: []});
		this.setState({questMessage: '',});
		
		axios.post(apiBaseUrl+'getQuests', null, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then(results => {
			if(results.status === 500)
			{
				localStorage.clear();
				window.location.reload();
			}
			else {
				return results;
			}
		})
		.then(data => {
			let questionnaires = data.data.data.map((quest) => {
				//console.log(JSON.stringify(quest));
				return(
					<div key={quest.id} id={'quest-'+quest.id} className="quest-single">
						<div>
							<h4><b>Naslov: {quest.title}</b></h4>
							<p className="text-muted">{quest.description}</p>
						</div>
						<Button variant="contained" color="secondary" onClick={(e) => this.deleteSingleQuestionnaire(quest.id, e)}>
					    	Delete
					    </Button>
					</div>
				)
			})
			this.setState({questionnaires: questionnaires});
			//console.log("state", this.state.questionnaires);
		})
		.catch(function (error) {
	      	console.log(error);
	    });

		this.setState({adminHome: false,addQ:false,deleteQ:true});
	}

	deleteSingleQuestionnaire(id, e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      "questId":id,
	    }

	    axios.post(apiBaseUrl+'deleteSingleQuest', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
    	.then((response) => {
    		if(response.status === 200){
	    		this.setState({
	    			questMessage: 'Questionnaire successfully deleted!',
	    			questMessageColor: 'green',
	    		});

	    		document.getElementById('quest-'+id).style.display = 'none';
    		}
    		else {
    			this.setState({
    				questMessage: 'Error occured',
    				questMessageColor: 'red'
    			})
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});
	}

	addNewQuestionnaire(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      "questTitle":this.state.questTitle,
	      "questDescription":this.state.questDescription
	    }

	    if(this.state.questTitle !== null && this.state.questTitle !== '') {
		    axios.post(apiBaseUrl+'addQuestionnaire', payload, {
				headers: { 
					'token': localStorage.getItem('token')
				}
			})
			.then((response) => {
	    		if(response.status === 200){
		    		this.setState({
		    			showQuest: false,
						showQuestion: true,
						showAnswer: false,
						questId: response.data.questId
		    		});
	    		}
	    		else {
	    			console.log('Error occured');
	    		}  		
	    	})
	    	.catch((error) => {
	      		console.log(error);
	    	});
	    }
	    else {
	    	this.setState({newQuestionnaireMessage: 'Please enter questionnaire title.'});
	    }
	}

	addNewQuestion(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      'question':this.state.question,
	      'questType':this.state.type,
	      'questId':this.state.questId
	    }

	    axios.post(apiBaseUrl+'addQuestion', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then((response) => {
    		if(response.status === 200){
	    		if(this.state.type === 'single'){
					this.setState({
		    			showQuest: false,
						showQuestion: false,
						showAnswer: true,
						questionId: response.data.questionId
	    			});
	    		}
	    		else {
	    			this.setState({
		    			showQuest: false,
						showQuestion: true,
						showAnswer: false,
						question: '',
						type: '',
						addQuestionMessage: 'Question added. Add another one?',
						questionId: response.data.questionId
	    			});
	    		}
	    		
    		}
    		else {
    			console.log('Error occured');
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});
	}

	addNewQuestionComplete(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      'question':this.state.question,
	      'questType':this.state.type,
	      'questId':this.state.questId
	    }

	    axios.post(apiBaseUrl+'addQuestion', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then((response) => {
    		if(response.status === 200){
	    		if(this.state.type === 'single'){
					this.setState({
		    			showQuest: false,
						showQuestion: false,
						showAnswer: true,
						questionId: response.data.questionId
	    			});
	    		}
	    		else {
	    			this.setState({adminHome:true,addQ:false,deleteQ:false});
	    		}
    		}
    		else {
    			console.log('Error occured');
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});
	}

	addNewAnswer(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      'answer':this.state.answerSingle,
	      'questId':this.state.questId,
	      'questionId':this.state.questionId
	    }

	    axios.post(apiBaseUrl+'addAnswer', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then((response) => {
    		if(response.status === 200){
    			console.log('success');
    		}
    		else {
    			console.log('Error occured');
    		}  		
    	})
    	.catch((error) => {
      		console.log(error);
    	});

    	this.state.answers.push(this.state.answerSingle);

    	let answersFormated = this.state.answers.map((answ) => {
			return(
				<div>
					<p>{answ}</p>
				</div>
			)
		})
		this.setState({answersFormated: answersFormated});

    	this.state.answerSingle = '';
	}

	addNewAnswerAddQuestion(e) {
		var apiBaseUrl = "http://localhost:4000/users/";
	    var payload={
	      'answer':this.state.answerSingle,
	      'questId':this.state.questId,
	      'questionId':this.state.questionId
	    }

	    if(this.state.answerSingle!==null) {
		    axios.post(apiBaseUrl+'addAnswer', payload, {
				headers: { 
					'token': localStorage.getItem('token')
				}
			})
			.then((response) => {
	    		if(response.status === 200){
	    			this.setState({
						showQuest: false,
						showQuestion: true,
						showAnswer: false,
						question: '',
						type: ''
					});
	    		}
	    		else {
	    			console.log('Error occured');
	    		}  		
	    	})
	    	.catch((error) => {
	      		console.log(error);
	    	});
	    }
	    else {
	    	this.setState({
				showQuest: false,
				showQuestion: true,
				showAnswer: false,
				question: '',
				type: ''
			});
	    }
	}

	changeQuestionType(e) {
		this.setState({type: e.target.value});
		if(e.target.value === 'single') {
			this.setState({addAnswers: true});
		}
		else {
			this.setState({addAnswers: false});
		}
	}

	goBack(e) {
		this.setState({
			adminHome: true,
			addQ:false,
			deleteQ:false
		})
	}

	render() {
		if (this.state.adminHome) {
			return (
				<MuiThemeProvider>
					<AppBar
			        	title="Admin Panel"
			        >
			        	<Toolbar>
			        		<Logout/>
			        	</Toolbar>
			        </AppBar>
					<div className="adminscreen">
						<div className="container" style={{marginTop: '20px'}}>
							<Button variant="contained" style={style} color="primary" onClick={(e) => this.addQuestionnaire(e)}>
						    	Add new questionnaire
						    </Button>
						    <Button variant="contained" style={style} color="primary" onClick={(e) => this.deleteQuestionnaire(e)}>
						    	Delete existing questionnaire
						    </Button>
						</div>
					</div>
				</MuiThemeProvider>
			)
		}
		else if(this.state.addQ) {
			if(this.state.showQuest) {
				return (
					<MuiThemeProvider>
						<AppBar
				        	title="Add new questionnaire"
				        >
				        	<Toolbar>
				        		<Button 
				        			variant="contained" 
				        			color="primary"
				        			onClick={(e) => this.goBack(e)}
				        			style={{marginRight: '10px'}}
				        		>
				        			Back
				        		</Button>
				        		<Logout/>
				        	</Toolbar>
				        </AppBar>
						<div className="adminscreen">
							<div className="container" style={{marginTop: '20px'}}>
								<TextField
					              	hintText="Enter questionnaire title"
					              	floatingLabelText="Title"
					              	onChange = {(event,newValue) => this.setState({questTitle:newValue})}
					            />
					            <br/>
					            <TextField
					                hintText="Enter questionnaire description"
					                floatingLabelText="Description"
					              	onChange = {(event,newValue) => this.setState({questDescription:newValue})}
					            />
					            <br/>
					            <br/>
					            <p>{this.state.newQuestionnaireMessage}</p>
					            <br/>
					            <Button 
				        			variant="contained" 
				        			color="primary"
				        			onClick={(e) => this.addNewQuestionnaire(e)}
				        		>
				        			Next >
				        		</Button>
							</div>
						</div>
					</MuiThemeProvider>
				)
			}
			else if(this.state.showQuestion) {
				return (
					<MuiThemeProvider>
						<AppBar
				        	title="Add new question"
				        >
				        	<Toolbar>
				        		<Button 
				        			variant="contained" 
				        			color="primary"
				        			onClick={(e) => this.goBack(e)}
				        			style={{marginRight: '10px'}}
				        		>
				        			Back
				        		</Button>
				        		<Logout/>
				        	</Toolbar>
				        </AppBar>
						<div className="adminscreen">
							<div className="container" style={{marginTop: '20px'}}>
								<h2>{this.state.questTitle}</h2>
								<h5>{this.state.questDescription}</h5>
								<TextField
					              	hintText="Enter a question"
					              	floatingLabelText="Question"
					              	onChange = {(event,newValue) => this.setState({question:newValue})}
					              	value = {this.state.question}
					            />
					            <br/>
					            <FormControl component="fieldset" required style={{marginTop: '20px'}}>
									<FormLabel component="legend">Type of question</FormLabel>
									<RadioGroup
										aria-label='Type'
										name='type'
										onChange={(event) => this.changeQuestionType(event)}
										value={this.state.type}
									>
										<FormControlLabel value='text' control={<Radio />} label='Text' />
										<FormControlLabel value='yes-no' control={<Radio />} label='Yes-No' />
										<FormControlLabel value='single' control={<Radio />} label='Predefined answers' />
									</RadioGroup>
								</FormControl>
					            <br/>
					            <br/>
					            <p>{this.state.addQuestionMessage}</p>
					            <br/>
					            <Button 
				        			variant="contained" 
				        			color="primary"
				        			onClick={(e) => this.addNewQuestion(e)}
				        			style={{marginRight: '10px'}}
				        		>
				        			Add question
				        		</Button>
				        		<Button 
				        			variant="contained" 
				        			color="primary"
				        			onClick={(e) => this.addNewQuestionComplete(e)}
				        		>
				        			Submit questionnaire
				        		</Button>
							</div>
						</div>
					</MuiThemeProvider>
				)
			}
			else if(this.state.showAnswer) {
				if(this.state.type === 'single') {
					return (
						<MuiThemeProvider>
							<AppBar
					        	title="Add answers"
					        >
					        	<Toolbar>
					        		<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.goBack(e)}
					        			style={{marginRight: '10px'}}
					        		>
					        			Back
					        		</Button>
					        		<Logout/>
					        	</Toolbar>
					        </AppBar>
							<div className="adminscreen">
								<div className="container" style={{marginTop: '20px'}}>
									<h2>{this.state.questTitle}</h2>
									<h5>{this.state.questDescription}</h5>
									<hr/>
									<h3>{this.state.question}</h3>
									<br/>
									{this.state.answersFormated}
									<br/>
									<br/>
									<TextField
						              	hintText="Enter an answer"
						              	floatingLabelText="Answer"
						              	onChange = {(event,newValue) => this.setState({answerSingle:newValue})}
						              	value = {this.state.answerSingle}
						            />
									<br/>
									<br/>
									<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.addNewAnswer(e)}
					        			style={{marginBottom: '10px'}}
					        		>
					        			Add another answer
					        		</Button>
					        		<br/>
					        		<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.addNewAnswerAddQuestion(e)}
					        			style={{marginBottom: '10px'}}
					        		>
					        			Add another question
					        		</Button>
					        		<br/>
									<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.completeQuestionnaire(e)}
					        		>
					        			Submit
				        		</Button>
								</div>
							</div>
						</MuiThemeProvider>
					);
				}
				else {
					return (
						<MuiThemeProvider>
							<AppBar
					        	title="Add new question"
					        >
					        	<Toolbar>
					        		<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.goBack(e)}
					        			style={{marginRight: '10px'}}
					        		>
					        			Back
					        		</Button>
					        		<Logout/>
					        	</Toolbar>
					        </AppBar>
							<div className="adminscreen">
								<div className="container" style={{marginTop: '20px'}}>
									<h2>{this.state.questTitle}</h2>
									<h5>{this.state.questDescription}</h5>
									<br/>
									<br/>
									<Button 
					        			variant="contained" 
					        			color="primary"
					        			onClick={(e) => this.completeQuestionnaire(e)}
					        		>
					        			Submit
				        		</Button>
								</div>
							</div>
						</MuiThemeProvider>
					)
				}
			}
		}
		else if(this.state.deleteQ) {
			return (
				<MuiThemeProvider>
					<AppBar
			        	title="Delete questionnaires"
			        >
			        <Toolbar>
			        	<Button 
		        			variant="contained" 
		        			color="primary"
		        			onClick={(e) => this.goBack(e)}
		        			style={{marginRight: '10px'}}
		        		>
		        			Back
		        		</Button>
		        		<Logout/>
		        	</Toolbar>
			        </AppBar>
					<div className="quest-container container">
						<p style={{color: this.state.questMessageColor, marginTop: '20px'}}>{this.state.questMessage}</p>
						{this.state.questionnaires}
					</div>
				</MuiThemeProvider>
			)
		}
	}
}

const style = {
  margin: 15,
};

export default AdminScreen;