class Quests extends Component {
	constructor() {
		super();

		this.state = {
			singlequest: []
		}
	};

	componentDidMount() {
		var apiBaseUrl = "http://localhost:4000/users/";
		var payload={
	      "questId":id,
	    }
		
		axios.post(apiBaseUrl+'getSingleQuest', payload, {
			headers: { 
				'token': localStorage.getItem('token')
			}
		})
		.then(results => {
			//console.log(results);
			return results;
		})
		.then(data => {
			let questionnaires = data.data.data.map((quest) => {
				//console.log(JSON.stringify(quest));
				return(
					<div key={quest.id} className="quest-single">
						<div>
							<h4><b>Naslov: {quest.title}</b></h4>
							<p className="text-muted">{quest.description}</p>
						</div>
						<Button variant="contained" color="primary" onClick={(e) => this.takeQuestionnaire(quest.id, e)}>
					    	Take it
					    </Button>
					</div>
				)
			})
			this.setState({questionnaires: questionnaires});
			//console.log("state", this.state.questionnaires);
		})
		.catch(function (error) {
	      	console.log(error);
	    });
	}