import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Button from '@material-ui/core/Button';

class Logout extends Component {
	constructor(props) {
		super(props);

		this.state={
			
		}
	}

	logout(e) {
		console.log('logout');
		localStorage.clear();
		window.location.reload();
	}

	render() {
		return (
			<Button 
				variant="contained" 
				color="primary"
				onClick={(e) => this.logout(e)}
			>
				Logout
			</Button>
		)
	}
}

export default Logout;