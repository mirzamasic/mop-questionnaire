var express = require('express');
var users = express.Router();
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');

var token;

users.use(cors());

process.env.SECRET_KEY = 'mirzamasic';

// Register handler

users.post('/register', function(req, res) {
	var today = new Date();
	var appData = {
		'error': 1,
		'data': ''
	};

	var userData = {
		'first_name': req.body.first_name,
		'last_name': req.body.last_name,
		'email': req.body.email,
		'password': req.body.password,
	}

	database.connection.getConnection(function(err, connection) {
 		if (err) {
			appData['error'] = 1;
			appData['data'] = 'Internal Server Error';
			res.status(500).json(appData);
		} 
		else {
 			connection.query('INSERT INTO users SET ?', userData, function(err, rows, fields) {
 				if (!err) {
					appData.error = 0;
					appData['data'] = 'User registered successfully!';
 					res.status(200).json(appData);
 				} 
 				else {
 					appData['data'] = 'Error Occured!';
 					res.status(400).json(appData);
 				}
 			});
 			
 			connection.release();
 		}
 	});
});

// Login handler

users.post('/login', function(req, res) {
	var appData = {};
 	var email = req.body.email;
 	var password = req.body.password;
	
	database.connection.getConnection(function(err, connection) {
 		if (err) {
 			appData['error'] = 1;
 			appData['data'] = 'Internal Server Error';
 			res.status(500).json(appData);
 		} 
 		else {
 			connection.query('SELECT * FROM users WHERE email = ?', [email], function(err, rows, fields) {
	 			if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				res.status(400).json(appData);
	 			} else {
	 				if (rows.length > 0) {
	 					if (rows[0].password == password) {
	 						token = jwt.sign(rows[0], process.env.SECRET_KEY, {
	 							expiresIn: 5000
	 						});
	 						appData['error'] = 0;
	 						appData['token'] = token;
	 						if (rows[0].isAdmin == 1) {
	 							appData['isAdmin'] = true;
	 						}
	 						else {
	 							appData['isAdmin'] = false;
	 						}
	 						res.status(200).json(appData);
	 					} else {
	 						appData['error'] = 1;
	 						appData['data'] = 'Email and Password does not match';
	 						res.status(204).json(appData);
	 					}
	 				} 
	 				else {
	 					appData['error'] = 1;
	 					appData['data'] = 'Email does not exists!';
	 					res.status(204).json(appData);
	 				}
	 			}
	 		});

 			connection.release();
 		}
 	});
});

// Check session

users.post('/checkSession', function(req, res) {
	var appData = {};
 	var token = req.body.token || req.headers['token'];

 	var user = jwt.decode(token, process.env.SECRET_KEY);

 	appData['error'] = 0;
 	appData['Token is valid'];

 	if(user.isAdmin == 1) {
 		appData['isAdmin'] = true;
 	}
 	else {
 		appData['isAdmin'] = false;
 	}

 	res.status(200).json(appData);
 });

// Authorization of tokens

users.use(function(req, res, next) {
	var token = req.body.token || req.headers['token'];
 	var appData = {};
 	
 	if (token) {
 		jwt.verify(token, process.env.SECRET_KEY, function(err) {
 			if (err) {
 				appData['error'] = 1;
 				appData['data'] = 'Token is invalid';
 				res.status(500).json(appData);
 			} 
 			else {
 				next();
 			}
 		});
 	} 
 	else {
 		appData['error'] = 1;
 		appData['data'] = 'Please send a token';
 		res.status(403).json(appData);
 	}
});

// get questionnaires

users.post('/getQuests', function(req, res) {
	var token = req.body.token || req.headers['token'];
	var appData = {};
	
	database.connection.getConnection(function(err, connection) {
	 	if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('SELECT * FROM questionnaires', function(err, rows, fields) {
	 			if (!err) {
	 				appData['error'] = 0;
	 				appData['data'] = rows;
	 				appData['message'] = 'Data returned';
	 				res.status(200).json(appData);
	 			} else {
	 				appData['data'] = 'No data found';
	 				res.status(204).json(appData);
	 			}
	 		});
	 		connection.release();
	 	}
 	});
});

// delete single questionnaire

users.post('/deleteSingleQuest', function(req, res) {
	var appData = {};
	var questId = req.body.questId;

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('DELETE FROM questionnaires WHERE id = ?', [questId], function(err, result, fields) {
				if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				res.status(400).json(appData);
	 			} else {
	 				appData['error'] = 0;
	 				appData['data'] = 'Questionnaire deleted!';
	 				res.status(200).json(appData);
	 			}
	 		});
	 	}
	});
});

// add questionnaire

users.post('/addQuestionnaire', function(req, res) {
	var appData = {};

	var data = {
		title: req.body.questTitle,
		description: req.body.questDescription
	}

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('INSERT INTO questionnaires SET ?', data, function(err, result, fields) {
				if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				appData['error-desc'] = err;
	 				res.status(400).json(appData);
	 			} else {
	 				appData['error'] = 0;
	 				appData['data'] = 'Questionnaire added!';
	 				appData['questId'] = result.insertId;
	 				res.status(200).json(appData);
	 			}
	 		});
	 	}
	});
});

// add question

users.post('/addQuestion', function(req, res) {
	var appData = {};

	var data = {
		questionnaire_id: req.body.questId,
		question: req.body.question,
		type: req.body.questType
	}

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('INSERT INTO questions SET ?', data, function(err, result, fields) {
				if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				appData['error-desc'] = err;
	 				res.status(400).json(appData);
	 			} else {
	 				appData['error'] = 0;
	 				appData['data'] = 'Question added!';
	 				appData['questionId'] = result.insertId;
	 				res.status(200).json(appData);
	 			}
	 		});
	 	}
	});
});

// add answer

users.post('/addAnswer', function(req, res) {
	var appData = {};

	var data = {
		question_id: req.body.questionId,
		answer: req.body.answer,
		questionnaire_id: req.body.questId,
	}

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('INSERT INTO answers SET ?', data, function(err, result, fields) {
				if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				appData['error-desc'] = err;
	 				res.status(400).json(appData);
	 			} else {
	 				appData['error'] = 0;
	 				appData['data'] = 'Answer added!';
	 				appData['answerId'] = result.insertId;
	 				res.status(200).json(appData);
	 			}
	 		});
	 	}
	});
});


// get single questionnaire

users.post('/getSingleQuest', function(req, res) {
	var token = req.body.token || req.headers['token'];
	var appData = {};

	var user = jwt.decode(token, process.env.SECRET_KEY);
	var questId = req.body.questId;

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('SELECT * FROM results WHERE questionnaire_id = ? AND user_id = ?', [questId, user.id] ,function(err, rows0, fields) {
				if (err) {
	 				appData['error'] = 1;
	 				appData['data'] = 'Error Occured!';
	 				res.status(400).json(appData);
	 			} else {
	 				if (rows0.length > 0) {
	 					appData['error'] = 1;
	 					appData['data'] = 'Already completed the questionnaire';
	 					res.status(204).json(appData);
	 				} else {
				 		connection.query('SELECT * FROM questionnaires WHERE id = ?; SELECT * FROM questions WHERE questionnaire_id = ?; SELECT * FROM answers WHERE questionnaire_id = ?', [questId, questId, questId] ,function(err, results, fields) {
				 			if (!err) {
				 				appData['error'] = 0;
				 				appData['questionnaire'] = results[0][0];

				 				var questions = [];

				 				results[1].forEach(function(elem, index){
				 					questions[index] = elem;
				 					questions[index]['answers'] = [];

				 					results[2].forEach(function(elem2, index2){
				 						if(elem2.question_id == elem.id) {
				 							questions[index]['answers'].push(elem2);
				 						}
				 					});
				 				});

				 				appData['questions'] = questions;

				 				res.status(200).json(appData);
				 			} else {
				 				appData['data'] = 'No data found';
				 			}
				 		});		 		
	 				}
	 			}
	 		});

	 		connection.release();
	 	}
	});
});

// save answers to database

users.post('/saveAnswers', function(req, res) {
	var token = req.body.token || req.headers['token'];
	var appData = {};

	var user = jwt.decode(token, process.env.SECRET_KEY);
	var answers = req.body.answers;

	var userData2 = [];

	answers.forEach(function(elem) {
		var question = elem[0].split('-');

		userData2.push([user.id, parseInt(question[1]),elem[1],req.body.questId]);
	});

	database.connection.getConnection(function(err, connection) {
		if (err) {
	 		appData['error'] = 1;
	 		appData['data'] = 'Internal Server Error';
	 		res.status(500).json(appData);
	 	} else {
	 		connection.query('INSERT INTO results (user_id, question_id, answer, questionnaire_id) VALUES ?', [userData2], function(err, rows, fields) {
 				if (!err) {
					appData.error = 0;
					appData['data'] = 'Answers saved successfully!';
 					res.status(200).json(appData);
 				} 
 				else {
 					appData['data'] = 'Error Occured!';
 					res.status(400).json(appData);
 				}
 			});
	 	}

	 	connection.release();
	});
});

module.exports = users;